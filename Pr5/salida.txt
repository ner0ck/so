#include <stdio.h>
#include <pthread.h>

void *hilo(void *arg)
{
    printf("Soy el hilo 139819707534224.\n", pthread_self());
    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_t id;
    
    pthread_create(&id, NULL, hilo, NULL);
    pthread_join(id, NULL);
    
    return 0;
}