#include <stdio.h>
#include <pthread.h>

void *hilo(void *arg)
{
    FILE *fich;
    const char *s = arg;
    
    fich = fopen(s, "r");
    printf("Fichero '%s' : ", s);

    if (fich)
        printf("Exito\n");
    else
        printf("Error\n");

    return fich;
}

int main(int argc, char *argv[])
{
    int nargs = argc-1;
    int ficheros = 0;
    pthread_t id[nargs];
    int i, j;
    void *fich;

    for (i=0; i<nargs; i++) {
        if (pthread_create(&id[i], NULL, hilo, argv[i+1]) != 0) {
            perror("error creando hilo");
            break;
        }
    }
    for (j=0; j<i; j++) {
        pthread_join(id[j], &fich);
        if (!fich)
            ficheros++;
    }
    
    return ficheros;
}