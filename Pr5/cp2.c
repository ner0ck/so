#include <stdio.h>
#include <pthread.h>

void *hilo(void *arg)
{
    printf("Hilo: mi identificador es %lu.\n", pthread_self());
    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_t id;
    
    printf("Principal: voy a crear un hilo.\n");
    pthread_create(&id, NULL, hilo, NULL);
    pthread_join(id, NULL);
    printf("Principal: el hilo %lu ha terminado.\n", id);
    
    return 0;
}