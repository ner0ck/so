#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>

#include "buffer.h"

#define TAM_LINEA 1024

/***
 *** Datos compartidos
 ***/
/* buffer compartido por productores y consumidores */
buffer_t *b;

/* mutex para exclusión mutua */
pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;

/* variable condición para que esperen los productores */
pthread_cond_t cond_prod = PTHREAD_COND_INITIALIZER;

/* variable condición para que esperen los consumidores */
pthread_cond_t cond_cons = PTHREAD_COND_INITIALIZER;

/* indicador para que los consumidores sepan que no quedan productores y
 * que, por lo tanto, cuando el buffer se vacía deben terminar
 * (no deben terminar inmediatamente, porque entonces podrían quedarse
 * sin tratar las últimas líneas depositadas por los productores) */
int terminar = 0;

/***
 *** Funciones base de los hilos
 ***/
/*
 * función base de los productores
 * - cada productor copia línea a línea un fichero en el buffer compartido
 * - el argumento es el puntero al nombre del fichero del que leer
 */
void *productor(void *arg)
{
    FILE *fich;
    const char *s = arg;
    char linea[TAM_LINEA];
    char *copia;

    printf("Productor: %lu - Fichero de entrada: '%s'\n", pthread_self(), s);
    fich = fopen(s, "r");

    if (fich)
    {
        while(fgets(linea, TAM_LINEA, fich))
        {
            copia = strdup(linea);

            pthread_mutex_lock(&mtx);
            while(buf_lleno(b))
            {
                pthread_cond_wait(&cond_prod, &mtx);
            }
            
            buf_poner(b, copia);

            pthread_cond_signal(&cond_cons);
            pthread_mutex_unlock(&mtx);
        }
        fclose(fich);
    }
    else
        perror("Error fichero");

    return NULL;
}

/*
 * función base de los consumidores
 * - cada consumidor va volcando línea a línea el contenido del buffer
 *   en el fichero cuyo nombre se le pasa como parámetro
 */
void *consumidor(void *arg)
{
    const char *s = arg;

    printf("Consumidor: %lu - Fichero de salida: '%s'\n", pthread_self(), s);

    return NULL;
}

/***
 *** Programa principal
 ***/
/*
 * función para indicar cómo utilizar el programa
 */
static void uso(const char *prog)
{
	fprintf(stderr,
	        "Uso: %s fich_de_salida fich_de_entrada_1 [f2 [...]]\n",
	        prog);
	exit(1);
}

/*
 * función base del hilo principal del programa
 */
int main(int argc, char *argv[])
{
    int prodTam = argc - 2;
    int i, j;

    pthread_t cons;
    pthread_t prods[prodTam];

    // Comprobar si se ha invocado el programa correctamente
    if (argc <= 2)
    {
        uso(argv[0]);
        return 1;
    }

    // Crear el buffer
    b = buf_crear(3);

    for(i = 0; i < prodTam; i++)
    {
        if (pthread_create(&prods[i], NULL, productor, argv[i + 2]))
        {
            perror("Error creando hilo");
            break;
        }
    }
    pthread_create(&cons, NULL, consumidor, argv[1]);

    for(j = 0; j < i; j++)
        pthread_join(prods[j], NULL);

    pthread_join(cons, NULL);

    buf_imprimir(b);

    return 0;
}