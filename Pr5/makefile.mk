CFLAGS  = -Wall -ggdb

# Objetivos para ejercicios (ej1.c, ej2.c, ...)
# El nombre del objetivo es el del fichero fuente sin la extensión
TARGETS_EJ = $(basename $(wildcard ej[0-9]*.c))

# Objetivos para ejercicios previos (ejprev1.c, ejprev2.c, ...)
# El nombre del objetivo es el del fichero fuente sin la extensión
TARGETS_EJPREV = $(basename $(wildcard ejprev[0-9]*.c))

# Todos los objetivos
TARGETS = $(TARGETS_EJ) $(TARGETS_EJPREV)

# Es necesario incluir la librería de hilos en todos los casos
LIBS = -lpthread

# Material de apoyo que debe ser enlazado con los ejercicios (en los previos no hace falta)
COMMON_EJ = buffer.o

# Objetivos que no se satisfacen aunque haya un fichero con su nombre
.PHONY : all clean cleanall

all : $(TARGETS)

# Los prerrequisitos de cada objetivo son su fichero fuente y el makefile
# Ver Static Pattern Rules en info:make
$(TARGETS_EJ) : % : %.o $(COMMON_EJ)
	 $(CC) $(CFLAGS) -o $@ $+ $(LIBS)

$(TARGETS_EJPREV) : % : %.o 
	 $(CC) $(CFLAGS) -o $@ $+ $(LIBS)


clean:
	-rm -f *~ *.bak *.o
	-rm -f *.txt
	
cleanall: clean
	-rm -f $(TARGETS)
