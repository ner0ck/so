#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "buffer.h"


buffer_t *buf_crear(int tam)
{
	buffer_t *b;

	if (tam <= 0) {
		fprintf(stderr, "buf_crear: tam debe ser mayor que 0\n");
		exit(1);
	}

	b = malloc(sizeof(buffer_t));
	b->lineas = malloc(sizeof(char *)*tam);
	assert(b->lineas);
	b->tam = tam;
	b->nelem = 0;
	b->poner = 0;
	b->quitar = 0;

	return b;
}

int buf_lleno(buffer_t *b)
{
	return b->nelem == b->tam;
}

int buf_vacio(buffer_t *b)
{
	return b->nelem == 0;
}

static void incrementa_poner(buffer_t *b)
{
	b->poner++;
	if (b->poner == b->tam) {
		b->poner = 0;
	}
}

static void incrementa_quitar(buffer_t *b)
{
	b->quitar++;
	if (b->quitar == b->tam) {
		b->quitar = 0;
	}
}

void buf_poner(buffer_t *b, char *s)
{
	if (buf_lleno(b)) {
		fprintf(stderr, "buf_poner: �el buffer est� lleno!\n");
		exit(1);
	}
	b->lineas[b->poner] = s;
	incrementa_poner(b);
	b->nelem++;
}

char *buf_quitar(buffer_t *b)
{
	char *s;

	if (buf_vacio(b)) {
		fprintf(stderr, "buf_quitar: �el buffer est� vac�o!\n");
		exit(1);
	}
	s = b->lineas[b->quitar];
	incrementa_quitar(b);
	b->nelem--;

	return s;
}

void buf_imprimir(buffer_t *b)
{
	int n, q;

	printf("Buffer @ %p: nelem %d tam %d poner %d quitar %d\n",
	       b, b->nelem, b->tam, b->poner, b->quitar);
	n = 0;
	q = b->quitar;
	while (n < b->nelem) {
		printf(" elem @ '%p': '%s'\n", b->lineas[q], b->lineas[q]);
		q++;
		if (q == b->tam) {
			q = 0;
		}
		n++;
	}
}
