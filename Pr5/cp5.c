#include <stdio.h>
#include <pthread.h>

void *hilo(void *arg)
{
    const char *s = arg;
    
    printf("Hilo: '%s'\n", s);
    return NULL;
}

int main(int argc, char *argv[])
{
    int nargs = argc-1;
    pthread_t id[nargs];
    int i, j;

    for (i=0; i<nargs; i++) {
        if (pthread_create(&id[i], NULL, hilo, argv[i+1]) != 0) {
            perror("error creando hilo");
            fprintf(stderr, "creados %d hilos\n", i);
            break;
        }
    }
    for (j=0; j<i; j++) {
        pthread_join(id[j], NULL);
    }
    
    return 0;
}