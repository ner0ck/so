#ifndef BUFFER_H
#define BUFFER_H

typedef struct {
	char **lineas;
	int poner;
	int quitar;
	int nelem;
	int tam;
} buffer_t;

buffer_t *buf_crear(int tam);
int buf_lleno(buffer_t *b);
int buf_vacio(buffer_t *b);
void buf_poner(buffer_t *b, char *s);
char *buf_quitar(buffer_t *b);
void buf_imprimir(buffer_t *b);

#endif /* BUFFER_H */
