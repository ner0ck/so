 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <buffer.h>

/* Para compilar este programa se debe usar la siguiente línea de órdenes
 * 
 *     gcc -Wall -I. -o bufferiza bufferiza.c buffer.c
 *
 * Los ficheros buffer.h y buffer.c tienen que estar en la misma carpeta
 * que este fichero.
 */

#define TAM_LINEA 1024

/* en la práctica buf tiene que ser una variable global,
 * aquí no haría falta
 */
buffer_t *buf;

/*
 * Función principal
 */
int main(int argc, char *argv[])
{
    char linea[TAM_LINEA];
    buf = buf_crear(3);
    char *s;
   
    /* Vamos almacenando líneas hasta que se llene el buffer */
    while (!buf_lleno(buf)) {
        /* recogemos una línea */
        printf("? ");
        fgets(linea, TAM_LINEA, stdin);
        
        /* hacemos una copia en memoria dinámica
         * si no, como buf_poner no la hace, todos los
         * elementos del buffer apuntarían a la misma cadena
         * (la última)
         */
        s = strdup(linea);
        
        /* guardamos en el buffer la dirección de la copia,
         * como buf_poner inserta al final, las líneas van quedando
         * en orden
         */
        buf_poner(buf, s);
    }
    printf("Se ha llenado el buffer.\n");
    buf_imprimir(buf);
   
    /* esperar a que quieran ver el contenido */
    printf("Presiona INTRO para continuar...");
    fgets(linea, TAM_LINEA, stdin);
    
    /* vamos mostrando las líneas hasta que el buffer se quede vacío */
    printf("Su contenido es el siguiente:\n");
    while (!buf_vacio(buf)) {
        /* se extrae la dirección de la cadena más antigua */
        s = buf_quitar(buf);
        printf("Línea: %s", s);
        
        /* como strdup hace internamente un malloc, es necesario hacer
         * free para recuperar el espacio
         */
        free(s);
    }
    
    return 0;
}
