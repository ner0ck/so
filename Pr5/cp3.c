#include <stdio.h>
#include <pthread.h>

void *hilo(void *arg)
{
    const char *prog = arg;
    
    printf("Hilo: el nombre del programa es '%s'.\n", prog);
    return (void *) 0x1234;
}

int main(int argc, char *argv[])
{
    pthread_t id;
    void *r;
    
    pthread_create(&id, NULL, hilo, argv[0]);
    pthread_join(id, &r);
    printf("Principal: el hilo ha devuelto %p.\n", r);
    
    return 0;
}