#include <stdio.h>
#include <pthread.h>

void *hilo(void *arg)
{
    const char *s = arg;
    
    printf("Hilo: '%s'\n", s);
    return NULL;
}

int main(int argc, char *argv[])
{
    int nargs = argc-1;
    pthread_t id[nargs];
    int i;

    for (i=0; i<nargs; i++) {
        pthread_create(&id[i], NULL, hilo, argv[i+1]);
    }
    for (i=0; i<nargs; i++) {
        pthread_join(id[i], NULL);
    }
    
    return 0;
}