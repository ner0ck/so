#include <stdio.h>
#include <stdlib.h>

struct Usuario
{
    char login[50], nombre[100];
    int uid;
};

struct Lista
{
    struct Usuario usuario;
    struct Lista *siguiente;
    struct Lista *primero;
};

int main(int argc, char *argv[])
{
    FILE *f;
    char *file;

    struct Lista *lista = NULL;
    struct Lista *ini = lista;
    struct Lista *nuevo;

    if(argc > 1)
        file = argv[1];
    else
        file = "/etc/passwd";

    f = fopen(file, "r");
    if (f == NULL)
    {
        perror("fopen");
        exit(1);
    }

    while(!feof(f))
    {
        
        nuevo = malloc(sizeof(struct Lista));

        fscanf(f, "%49[^:]:%*[^:]:%d:%*d:%99[^:]%*s%*c", nuevo->usuario.login, &(nuevo->usuario.uid), nuevo->usuario.nombre);
  
        if (ini == NULL)
            ini = lista = nuevo;
        else if (nuevo->usuario.uid < ini->usuario.uid)
        {
            nuevo->siguiente = ini;
            ini = nuevo;
        }
        else
        {
            while (lista->siguiente != NULL && lista->siguiente->usuario.uid < nuevo->usuario.uid)
                lista = lista->siguiente;

            nuevo->siguiente = lista->siguiente;
            lista->siguiente = nuevo;
        }

        lista = ini;
    }
    fclose(f);

    /* EJERCICIO OPCIONAL */
    lista = ini;
    while(lista->siguiente != NULL)
    {
        if (lista->usuario.login == lista->siguiente->usuario.login || lista->usuario.uid == lista->siguiente->usuario.uid)
        {
            nuevo = lista->siguiente;
            lista->siguiente = nuevo->siguiente;
            free(nuevo);
        }
        else
            lista = lista->siguiente;
    }

    lista = ini;
    while(lista != NULL)
    {
        printf("Usuario: %s\nNombre completo: %s\nIdentificador: %d\n\n", lista->usuario.login, lista->usuario.nombre, lista->usuario.uid);
        lista = lista->siguiente;
    }

    /* EJERCICIO 6 */
    while(ini != NULL)
    {
        nuevo = ini;
        ini = ini->siguiente;
        free(nuevo);
    }

    return 0;
}