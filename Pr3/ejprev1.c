#include <stdio.h>

int main(int argc, char *argv[])
{
    int i;

    printf("Hola soy el programa '%s'\nHe sido invocado con los siguientes argumentos:\n", argv[0]);
    for (i = 0; i < argc; i++)
        printf("\targv[%d] = '%s'\n", i, argv[i]);
    printf("En total son %d argumentos.\n", argc);
    if (argc <= 1)
        printf("He observado que no has incluido ningun argumento.\nSi quieres, puedes probar a poner argumentos y yo te los cuento :-)\n");
    printf("Hasta la proxima :-)\n");

    return 0;
}