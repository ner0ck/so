#include <stdio.h>

int calculo(int prim, int seg, int *sum, int *mult)
{
    *sum = prim + seg;
    *mult = prim * seg;

    return (prim >= seg) ? prim : seg;
}

int main(int argc, char *argv[])
{
    int valor1, valor2;
    int resultado, suma, producto;

    printf("Introduce 2 enteros separados entre sí por un espacio: ");
    scanf("%d %d", &valor1, &valor2);

    resultado = calculo(valor1, valor2, &suma, &producto);

    printf("Resultados obtenidos para los valores %d y %d.\n", valor1, valor2);
    printf("    Resultado: %d\n", resultado);
    printf("    Suma: %d\n", suma);
    printf("    Producto: %d\n", producto);

    return 0;
}