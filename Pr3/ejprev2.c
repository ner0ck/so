#include <stdio.h>

int main(int argc, char *argv[])
{
    int a, b, c;

    printf("Introduce 3 numeros enteros separados por espacios:\n");

    scanf("%d %d %d", &a, &b, &c);

    printf("La suma de %d, %d y %d es %d.\n", a, b, c, a+b+c);

    return 0;
}