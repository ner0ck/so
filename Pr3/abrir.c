#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    FILE *f;

    if (argc != 2)
    {
        printf("Uso ./abrir fichero\n");
        exit(1);
    }

    f = fopen(argv[1], "r");
    if (f == NULL)
    {
        perror("fopen");
        exit(2);
    }    
    fclose(f);

    printf("Se puede leer de %s.\n", argv[1]);

    return 0;
}