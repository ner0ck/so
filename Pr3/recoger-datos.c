#include <stdio.h>

struct dato
{
    char login[20], gecos[50];
    int uid;
};

int main(int argc, char *argv[])
{
    struct dato d;
    
    printf("--- Recogida de datos\n");
    
    printf("Introduce el nombre de usuario: ");
    scanf("%19[^\n]%*c", d.login);

    printf("Introduce el nombre completo: ");
    scanf("%49[^\n]%*c", d.gecos);

    printf("Introduce el identificador de usuario: ");
    scanf("%d", &d.uid);

    printf("--- Datos introducidos:\n");
    printf("\tUsuario: %s\n", d.login);
    printf("\tNombre completo: %s\n", d.gecos);
    printf("\tIdentificador: %d\n", d.uid);

    return 0;
}