#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    FILE *f;
    char *file;
    char login[50], gecos[100];
    int uid;

    if(argc > 1)
        file = argv[1];
    else
        file = "/etc/passwd";

    f = fopen(file, "r");
    if (f == NULL)
    {
        perror("fopen");
        exit(1);
    }

    printf("--- Fichero: '%s'\n\n", file);
    while(!feof(f))
    {
        if (fscanf(f, "%49[^:]:%*[^:]:%d:%*d:%99[^:]%*s%*c", login, &uid, gecos) > 0)
            printf("Usuario: %s\nNombre completo: %s\nIdentificador: %d\n\n", login, gecos, uid);
    }
    fclose(f);
    printf("--- Fin del fichero\n");

    return 0;
}