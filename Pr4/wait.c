#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{
    int i, r;
    pid_t pid;
    const int n = 5;

    for (i=0; i<n; ++i) {
        pid = fork();
        if (pid == 0) {
            printf("* Proceso %d-ésimo: PID=%u, PPID=%u\n",
                i, getpid(), getppid());
            exit(i);
        }
        printf("- Creado proceso %d-ésimo: PID=%u\n",
            i, pid);
    }

    for (i=0; i<n; ++i) {
        pid = wait(&r);
        printf("+ Finalizado proceso: PID=%u, r=%d\n",
            pid, WEXITSTATUS(r));
    }

    return 0;
}