#include <stdio.h>
#include <string.h>

#define MAX 1024

void limpiar(char *s)
{
    s[strlen(s) - 1] = '\0';
}

int main(int argc, char *argv[])
{
    char linea[MAX], *p;
    char *delimitadores = " ";
    int i = 1;

    printf("? ");
    fgets(linea, MAX, stdin);
    limpiar(linea);

    p = strtok(linea, delimitadores);
    while(p)
    {
        printf("Palabra %d: %s\n", i, p);
        p = strtok(NULL, delimitadores);
        i++;
    }

    return 0;
}