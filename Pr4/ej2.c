#include <stdio.h>
#include <string.h>

#define MAX 1024
#define WORDS 4096

void limpiar(char *s)
{
    s[strlen(s) - 1] = '\0';
}

int main(int argc, char *argv[])
{
    char linea[MAX], *p;
    char *cadena[WORDS + 1];
    cadena[WORDS + 1] = NULL;
    char *delimitadores = " ";
    int i = 0;

    printf("? ");
    fgets(linea, MAX, stdin);
    limpiar(linea);

    p = strtok(linea, delimitadores);
    while(p)
    {
        cadena[i] = p;
        p = strtok(NULL, delimitadores);
        i++;
    }

    i = 0;
    while(cadena[i] != NULL)
    {
        printf("Palabra %d: %s\n", i + 1, cadena[i]);
        i++;
    }

    return 0;
}