#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX 1024
#define WORDS 4096

void limpiar(char *s)
{
    s[strlen(s) - 1] = '\0';
}

int main(int argc, char *argv[])
{
    char linea[MAX], *p;
    char *cadena[WORDS + 1];
    cadena[WORDS + 1] = NULL;
    char *delimitadores = " ";
    int i = 0;
    int error;
    pid_t pid;

    printf("? ");
    fflush(stdout);
    fgets(linea, MAX, stdin);
    limpiar(linea);

    p = strtok(linea, delimitadores);
    while(p)
    {
        cadena[i] = p;
        p = strtok(NULL, delimitadores);
        i++;
    }

    pid = fork();
    if (pid == -1)
        perror("Error fork");
    else if (pid == 0)
        if (execvp(cadena[0], cadena) == -1)
            printf("El programa no existe.\n");
    wait(NULL);

    return 0;
}