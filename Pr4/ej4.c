#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX 1024
#define WORDS 4096

void limpiar(char *s)
{
    s[strlen(s) - 1] = '\0';
}

int main(int argc, char *argv[])
{
    char linea[MAX], *p;
    char *cadena[WORDS + 1];
    char *delimitadores = " ";
    int i;
    int salir = 0;
    pid_t pid;

    do
    {
        printf("? ");
        fflush(stdout);
        fgets(linea, MAX, stdin);
        limpiar(linea);
        
        i = 0;
        p = strtok(linea, delimitadores);
        while(p)
        {
            cadena[i] = p;
            p = strtok(NULL, delimitadores);
            i++;
        }
        cadena[WORDS + 1] = NULL;

        if (feof(stdin) || strcmp(cadena[0], "exit") == 0)
            salir = 1;

        if (salir != 1)
        {
            pid = fork();
            if (pid == -1)
                perror("Error fork");
            else if (pid == 0)
                if (execvp(cadena[0], cadena) == -1)
                    printf("El programa no existe.\n");
        }
        else
            exit(-1);

        wait(NULL);
        printf("\n");
    } while(salir != 1);

    return 0;
}