#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX_ARGS 256
#define MAX_LONG 1024

void quitar_nueva_linea(char *s)
{
    int l;

    l = strlen(s) - 1;
    if (s[l] == '\n') {
        s[l] = '\0';
    }
}

int main(int argc, char *argv[])
{
    char ejecutable[MAX_LONG];
    /* se incluye un elemento de más para el NULL 
     * que indica el final de la lista de argumentos */
    char *argumentos[MAX_ARGS + 1];
    char linea_argumentos[MAX_LONG];
    int n_argumentos;
    char *p;

    /* Leer el nombre del fichero ejecutable */
    puts("Datos del programa a ejecutar:");
    printf("- fichero ejecutable:\n  ? "); 
    fflush(stdout);
    if (fgets(ejecutable, MAX_LONG, stdin) == NULL) {
        fprintf(stderr, "No se puede leer el nombre del fichero ejecutable\n");
        exit(1);
    }
    quitar_nueva_linea(ejecutable);

    /* Leer la línea con los argumentos */
    printf("- lista de argumentos separados por comas (incluido el nombre del programa):\n  ? "); 
    fflush(stdout);
    if (fgets(linea_argumentos, MAX_LONG, stdin) == NULL) {
        fprintf(stderr, "No se pueden leer los argumentos\n");
        exit(1);
    }
    quitar_nueva_linea(linea_argumentos);
    
    /* Separar los argumentos y preparar el vector de punteros */
    n_argumentos = 0;
    p = strtok(linea_argumentos, ",");
    while (p != NULL && n_argumentos < MAX_ARGS) {
        argumentos[n_argumentos++] = p;
        p = strtok(NULL, ",");
    }
    argumentos[n_argumentos] = NULL;
    if (n_argumentos == 0) {
        fprintf(stderr, "No se ha introducido ningún argumento y argv[0] es imprescindible.\n");
        exit(1);
    }

    /* Ejecutar el programa */
    printf("Ejecución del programa: %s\n", ejecutable);
    if (execvp(ejecutable, argumentos) == -1) {
        perror("execvp");
        exit(1);
    }

    /* Imprimir un mensaje */
    printf("\n:-)\n");

    return 0;
}