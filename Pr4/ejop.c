#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX 1024
#define WORDS 4096

void limpiar(char *s)
{
    s[strlen(s) - 1] = '\0';
}

int main(int argc, char *argv[])
{
    char linea[MAX], *p;
    char *cadena[WORDS + 1];
    char *delimitadores = " ";
    int i, j;
    int salir = 0;
    pid_t pid;

    do
    {
        printf("? ");
        fflush(stdout);
        fgets(linea, MAX, stdin);
        limpiar(linea);

        i = 0;
        p = strtok(linea, delimitadores);
        while(p)
        {
            cadena[i] = p;
            p = strtok(NULL, delimitadores);
            i++;
        }
        cadena[WORDS + 1] = NULL;

        if (feof(stdin) || strcmp(cadena[0], "exit") == 0)
            salir = 1;

        if (salir != 1)
        {
            i = 0;
            while(cadena[i] != NULL)
            {
                if (cadena[i][0] == '$')
                {
                    char *var = &cadena[i][1];
                    cadena[i] = getenv(var);
                }
                i++;
            }
            
            if (strcmp(cadena[0], "exec") == 0)
            {
                for (j = 0; j < i - 1; j++)
                    cadena[j] = cadena[j + 1];
                cadena[i - 1] = NULL;

                if (execvp(cadena[0], cadena) == -1)
                        perror("Error exec");
            }
            else if (strcmp(cadena[0], "cd") == 0)
            {
                if (chdir(cadena[1]) == -1)
                    perror("Error cd");
            }
            else if (strcmp(cadena[0], "export") == 0)
            {
                if (cadena[1] == NULL)
                    perror("Error export");
                else
                {
                    char *export[2];
                    export[0] = strtok(cadena[1], "=");
                    export[1] = strtok(NULL, "=");
                    if (setenv(export[0], export[1], 1) != 0)
                        perror("Error export");
                }
            }
            else
            {
                pid = fork();
                if (pid == -1)
                    perror("Error fork");
                else if (pid == 0)
                    if (execvp(cadena[0], cadena) == -1)
                        perror("Error");
            }
        }
        else
            exit(-1);

        wait(NULL);
        printf("\n");
    } while(salir != 1);

    return 0;
}