#include <stdio.h>
#include <string.h>

#define MAX_LONG 1024

void quitar_nueva_linea(char *s)
{
    int l;

    l = strlen(s) - 1;
    if (s[l] == '\n') {
        s[l] = '\0';
    }
}

int main(int argc, char *argv[])
{
    char linea[MAX_LONG];
    char *delimitadores = ",;.";
    char *p;
    int i = 1;


    printf("Línea: ");
    fgets(linea, MAX_LONG, stdin);
    quitar_nueva_linea(linea);

    printf("\n---\n\nlinea: '%s'\n\n", linea);
    p = strtok(linea, delimitadores);
    while (p) {
        printf("- token %d: '%s'\n", i, p);
        p = strtok(NULL, delimitadores);
        i++;
    }

    return 0;
}