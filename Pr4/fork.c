#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int i, j, r;
    const int N = 2;
    const int M = 3;

    for (i=0; i<N; ++i) {
        for (j=0; j<M; ++j) {
            r = fork();
            if (r < 0) {
                perror("fork ha fallado");
                exit(1);
            }
        }
    }
    printf("¡Hola mundo!\n");
    return 0;
}