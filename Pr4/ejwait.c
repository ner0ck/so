#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{
    int i, r;
    pid_t pid;
    char *palabra[2] = {"primer", "segundo"};

    for(i = 0; i < 2; i++)
    {
        pid = fork();

        if (pid == 0)
        {
            if (i == 0)
                sleep(5);
            else
                sleep(3);

            exit(i);
        }
    }

    for(i = 0; i < 2; i++)
    {
        pid = wait(&r);

        printf("Termina el %s hijo, PID = %d.\n", palabra[WEXITSTATUS(r)], pid);
    }

    return 0;
}