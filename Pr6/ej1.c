#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>

#define TAM_BUF 1024

int main(int argc, char *argv[])
{
    int i;
    int nlee;
    int f[argc - 1];
    char buf[TAM_BUF];

    for (i = 1; i < argc; i++) {
        if ((f[i - 1] = open(argv[i], O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR)) < 0) {
            perror("open");
            exit(1);
        }
    }

    while((nlee = read(STDIN_FILENO, buf, TAM_BUF)) > 0) {
        write(STDOUT_FILENO, buf, nlee);
        for (i = 0; i < argc - 1; i++) {
            if ((write(f[i], buf, nlee)) < 0) {
                perror("write");
                exit(1);
            }
        }
    }

    for (i = 0; i < argc - 1; i++)
        close(f[i]);

    exit(0);
}