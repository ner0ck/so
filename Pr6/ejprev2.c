#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
    int fd;
    char cadena[] = "Hola :-)\n";

    if (argc != 2) {
        fprintf(stderr, "Uso: %s fichero_de_salida\n", argv[0]);
        exit(1);
    }
    fd = open(argv[1], O_WRONLY | O_TRUNC | O_CREAT, S_IRUSR);
    if (fd < 0) {
        perror("open");
        exit(1);
    }
    if (write(fd, cadena, strlen(cadena))<0) {
        perror("write");
        exit(1);
    }
    close(fd);
    exit(0);
}