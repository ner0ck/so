#include <stdio.h>
#include <unistd.h>

#define TAM_BUF 1

int main(int argc, char *argv[])
{
    char buf[TAM_BUF];
    int nlee;

    while((nlee = read(STDIN_FILENO, buf, TAM_BUF)) > 0)
        write(STDOUT_FILENO, buf, nlee);

    return 0;
}