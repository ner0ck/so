#include <stdio.h>
#include <stdlib.h>

int main ()
{
    FILE * pFile;
    pFile = fopen ("myfile.txt","w");
    if (pFile == NULL) {
        perror("fopen myfile.txt");
        exit(1);
    }
    fputs ("ejemplo fopen\n",pFile);
    fclose (pFile);
    return 0;
}