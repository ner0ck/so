#include <stdio.h>
#include <dirent.h>

int listado(const char *nomdir)
{
    DIR * d;
    struct dirent * entrada;

    d=opendir(nomdir);
    if (d==NULL) {
        perror("Error al abrir el directorio");
        return -1;
    }

    printf("--- Listado del directorio: %s\n", nomdir);
    while ((entrada = readdir(d)) != NULL)  {
        printf("%s\n", entrada->d_name);
    }
    printf("\n");
    closedir(d);
    return 0;
}

int main(int argc, char *argv[]) {
    int i;

    if (argc < 2) {
        if (argc != 2) {
            fprintf(stderr, "Uso: %s directorio [dir2 [...]]\n", argv[0]);
            return 1;
        }
    }

    for (i = 1; i < argc; i++)
        listado(argv[i]);

    return 0;
}