#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define TAM_BUF 8192

int main(int argc, char *argv[])
{
    char buf[TAM_BUF];
    int fd_ent, fd_sal, nlee;
    int i, vacio = 0;

    /* abrir los ficheros */
    fd_ent = open(argv[1], O_RDONLY);
    if (fd_ent < 0) {
        perror("open");
        exit(1);
    }
    fd_sal = creat(argv[2], S_IRUSR | S_IWUSR);
    if (fd_sal < 0) {
        perror("creat");
        exit(1);
    }
    
    while((nlee = read(fd_ent, buf, TAM_BUF)) > 0) {
        for (i = 0; i < TAM_BUF; i++)
            if (buf[0] != 0)
                vacio = 1;
        
        if (vacio != 0) {
            write(fd_sal, buf, nlee);
            vacio = 0;
        }
        else
            lseek(fd_sal, nlee, SEEK_CUR);
    }
    
    /* finalización */
    close(fd_ent);
    close(fd_sal);
    return 0;
}