#include <stdio.h>
#include <dirent.h>
#include <limits.h>
#include <sys/stat.h>

void mostrarFichero(char *path)
{
    struct stat fich;
    if (lstat(path, &fich) >= 0) {
        if (S_ISREG(fich.st_mode))
            printf("\n%lu bytes -\t%lu bloques - %s", fich.st_size, fich.st_blocks, path);
    }
    else
        perror("lstat");
}

int listado(const char *nomdir)
{
    DIR * d;
    struct dirent * entrada;
    char str[PATH_MAX];

    d=opendir(nomdir);
    if (d==NULL) {
        perror("Error al abrir el directorio");
        return -1;
    }

    printf("--- Listado del directorio: %s\n", nomdir);
    while ((entrada = readdir(d)) != NULL)  {
        snprintf(str, PATH_MAX, "%s/%s", nomdir, entrada->d_name);
        mostrarFichero(str);
    }
    printf("\n");
    closedir(d);
    return 0;
}

int main(int argc, char *argv[]) {
    int i;

    if (argc < 2) {
        if (argc != 2) {
            fprintf(stderr, "Uso: %s directorio [dir2 [...]]\n", argv[0]);
            return 1;
        }
    }

    for (i = 1; i < argc; i++)
        listado(argv[i]);

    return 0;
}