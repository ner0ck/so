#include <stdio.h>

#define TAM_BUF 1

int main(int argc, char *argv[])
{
    char buf[TAM_BUF];
    size_t n_in;

    while( (n_in = fread(buf,sizeof(char), TAM_BUF, stdin)) > 0 ) {
            fwrite(buf, sizeof(char), n_in, stdout);
    }
    return 0;
}