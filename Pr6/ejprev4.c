#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    struct stat s;
    
    if (argc != 2) {
        fprintf(stderr, "Uso: %s fichero_de_interés\n", argv[0]);
        exit(1);
    }
    if (lstat(argv[1], &s) < 0) {
        perror("stat");
        exit(1);
    }
    printf("Información sobre: %s\n", argv[1]);
    if (S_ISDIR(s.st_mode)) {
        printf("- es un directorio\n");
    } else if (S_ISLNK(s.st_mode)) {
        printf("- es un enlace simbólico\n");
    } else if (S_ISBLK(s.st_mode)) {
        printf("- es un dispositivo de bloques\n");
    } else {
        printf("- nodo-i: %lu\n", s.st_ino);
        printf("- número de alias: %lu\n", s.st_nlink);
    }

    return 0;
}